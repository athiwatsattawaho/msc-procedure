create or replace PROCEDURE SP_QUERY_DOMEST_REGCOUNT
(                            pReport IN VARCHAR,
                             pFROM_DATE IN DATE,
                             pTO_DATE IN DATE,
                             pREGION_ID IN VARCHAR,
                             pAREA_ID IN VARCHAR,
                             pBRANCH_ID IN VARCHAR,
                             oRegLink OUT VARCHAR,
                             oRegCount OUT NUMBER
) AS
BEGIN
    DECLARE
          cusCount NUMBER :=NULL;
          tempCount NUMBER :=NULL;
    BEGIN
      SELECT COUNT(*) INTO cusCount FROM SP_CUS_IDS;
      FOR TEMP_AMT_TAX_C IN (
            select case when (RPD.GROUP_DESC = 'สุรา' and RPD.SUBGRP_NAME not in ( 'สุราแช่พื้นเมือง เช่น กระแช่หรือน้ำตาลเมา อุ น้ำขาว หรือสาโท','สุราแช่ชุมชน','สุรากลั่นชุมชน','เบียร์', 'เบียร์(Brewpub)')) then 'สุรา โรงใหญ่'
                         when (RPD.GROUP_DESC = 'สุรา' and RPD.SUBGRP_NAME in( 'สุรากลั่นชุมชน', 'สุราแช่ชุมชน')) then 'สุรา ชุมชน'
                         when (RPD.GROUP_DESC = 'สุรา' and RPD.SUBGRP_NAME in('เบียร์', 'เบียร์(Brewpub)')) then 'เบียร์'
                         else RPD.GROUP_DESC end as GROUP_DESC
                        ,case when RND.SUB_GROUP_DESC = 'ผู้ผลิต' then 1			
                         when RND.SUB_GROUP_DESC = 'บริการ' then 1			
                         when RND.SUB_GROUP_DESC = 'ผู้นำเข้า' then 2 else 3 end  as GroupTypeId		
                        , RND.SUB_GROUP_DESC
                        , count(distinct RND.NEW_REG_ID) as Amount			
            from ED_TARGET.IC_REGISTER_NW_DIM RND			
            inner join ED_TARGET.REGISTER_PRODUCT_DIM RPD on RPD.NEWREG_ID = RND.NEW_REG_ID		
            inner join ED_TARGET.IC_OFFICE_DIM OD on OD.OFFCODE=RND.OFFCODE			
            where   RND.ACTIVE_FLAG > '0'	        -- Fix
                    AND RPD.GROUP_DESC = 'เครื่องดื่ม' 	-- Fix 
                    AND RND.REG_DATE <= to_number(to_char(pTO_DATE, 'yyyymmdd')  )    -- Where Parameter @EndDate
                    AND OD.REGION_CD = NVL(pREGION_ID ,OD.REGION_CD)            -- Where Parameter
                    and OD.PROVINCE_CD = NVL(pAREA_ID ,OD.PROVINCE_CD)          -- Where Parameter
                    and RND.OFFCODE = NVL(pBRANCH_ID,RND.OFFCODE)            -- Where Parameter
		      and (RND.REG_ID IN (SELECT CUS_ID FROM SP_CUS_IDS) OR cusCount = 0)             -- Where Parameter
            group by (
                    case when (RPD.GROUP_DESC = 'สุรา' and RPD.SUBGRP_NAME not 
                            in ( 'สุราแช่พื้นเมือง เช่น กระแช่หรือน้ำตาลเมา อุ น้ำขาว หรือสาโท','สุราแช่ชุมชน','สุรากลั่นชุมชน','เบียร์', 'เบียร์(Brewpub)')) then 'สุรา โรงใหญ่'
                        when (RPD.GROUP_DESC = 'สุรา' and RPD.SUBGRP_NAME in( 'สุรากลั่นชุมชน', 'สุราแช่ชุมชน')) then 'สุรา ชุมชน'
                        when (RPD.GROUP_DESC = 'สุรา' and RPD.SUBGRP_NAME in('เบียร์', 'เบียร์(Brewpub)')) then 'เบียร์'
                        else RPD.GROUP_DESC end)
                    , (case when RND.SUB_GROUP_DESC = 'ผู้ผลิต' then 1			
                    when RND.SUB_GROUP_DESC = 'บริการ' then 1			
                    when RND.SUB_GROUP_DESC = 'ผู้นำเข้า' then 2 else 3 end)	
                    , RND.SUB_GROUP_DESC
                    
            order by (case when (RPD.GROUP_DESC = 'สุรา' and RPD.SUBGRP_NAME not 
                    in ( 'สุราแช่พื้นเมือง เช่น กระแช่หรือน้ำตาลเมา อุ น้ำขาว หรือสาโท','สุราแช่ชุมชน','สุรากลั่นชุมชน','เบียร์', 'เบียร์(Brewpub)')) then 'สุรา โรงใหญ่'
                        when (RPD.GROUP_DESC = 'สุรา' and RPD.SUBGRP_NAME in( 'สุรากลั่นชุมชน', 'สุราแช่ชุมชน')) then 'สุรา ชุมชน'
                        when (RPD.GROUP_DESC = 'สุรา' and RPD.SUBGRP_NAME in('เบียร์', 'เบียร์(Brewpub)')) then 'เบียร์'
                        else RPD.GROUP_DESC end)
                    , (case when RND.SUB_GROUP_DESC = 'ผู้ผลิต' then 1			
                    when RND.SUB_GROUP_DESC = 'บริการ' then 1			
                    when RND.SUB_GROUP_DESC = 'ผู้นำเข้า' then 2 else 3 end)
          )
           LOOP
                  IF TEMP_AMT_TAX_C.GroupTypeId = 1 --ผู้ผลิต
                    THEN tempCount := tempCount + TEMP_AMT_TAX_C.Amount;
                  End if;
            END LOOP;
           oRegCount:= tempCount;
          oRegLink:= 'https://bi.excise.go.th/analytics/saw.dll?Dashboard&PortalPath=%2Fshared%2FNEW_REPORT%2F%E0%B8%A3%E0%B8%B2%E0%B8%A2%E0%B8%87%E0%B8%B2%E0%B8%99%E0%B8%88%E0%B8%94%E0%B8%97%E0%B8%B0%E0%B9%80%E0%B8%9A%E0%B8%B5%E0%B8%A2%E0%B8%99%2F%E0%B8%A3%E0%B8%B2%E0%B8%A2%E0%B8%87%E0%B8%B2%E0%B8%99%E0%B8%9C%E0%B8%B9%E0%B9%89%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%AD%E0%B8%9A%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%88%E0%B8%94%E0%B8%97%E0%B8%B0%E0%B9%80%E0%B8%9A%E0%B8%B5%E0%B8%A2%E0%B8%99_v2&Page=page%201';
    END;
END SP_QUERY_DOMEST_REGCOUNT;