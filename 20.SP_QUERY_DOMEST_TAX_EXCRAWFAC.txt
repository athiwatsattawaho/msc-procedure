create or replace PROCEDURE SP_QUERY_DOMEST_TAX_EXCRAWFAC
(        pFROM_DATE IN DATE,
         pTO_DATE IN DATE,
         pREGION_ID IN VARCHAR,
         pAREA_ID IN VARCHAR,
         pBRANCH_ID IN VARCHAR,
--
         oAmtTaxExcRawFac OUT NUMBER,
         oVolTaxExcRawFac OUT NUMBER
)AS 

BEGIN
  DECLARE
        cusCount NUMBER := NULL;
    BEGIN
        SELECT COUNT(*) INTO cusCount FROM SP_CUS_IDS;
            FOR TEMP_AMT_TAX_A IN (
                    select    sum(nvl(saf.REDUCE_AMT,0))     AS  amtTaxExcRawFac --ภาษี
                            , sum(nvl(saf.REDUCE_VOL_AMT,0)) AS  volTaxExcRawFac --ปริมาณ
                    from ed_target.IC_SUM_ALLMTH_FACT saf
                    inner join ed_target.IC_PRODUCT_DIM pd on pd.product_code = saf.product_cd
                    left join ed_target.IC_OFFICE_DIM o on o.offcode = saf.offcode_own 
                    where  pd.group_id='0201'   -- Fix 
                           and saf.IMPORT_STATUS = 1    --Fix
                           and saf.time_id >= 20210701  -- Where Parameter @StartDate
                           and saf.time_id <= 20210731  -- Where Parameter @EndDate       
                            AND o.REGION_CD = ''        -- Where Parameter
                            and o.PROVINCE_CD = ''      -- Where Parameter
                            and saf.OFFCODE_OWN = ''    -- Where Parameter
                            and saf.reg_id = ''         -- Where Parameter
               )
            LOOP
                    dbms_output.put_line('TEMP_AMT_TAX_A');
                    oAmtTaxExcRawFac := TEMP_AMT_TAX_A.amtTaxExcRawFac;
                    oVolTaxExcRawFac := TEMP_AMT_TAX_A.volTaxExcRawFac;
            END LOOP;
    END;

END SP_QUERY_DOMEST_TAX_EXCRAWFAC;