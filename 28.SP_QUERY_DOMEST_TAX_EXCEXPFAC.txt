create or replace PROCEDURE SP_QUERY_DOMEST_TAX_EXCEXPFAC
(        pFROM_DATE IN DATE,
         pTO_DATE IN DATE,
         pREGION_ID IN VARCHAR,
         pAREA_ID IN VARCHAR,
         pBRANCH_ID IN VARCHAR,
--
         oAmtTaxExcExpFac OUT NUMBER,
         oVolTaxExcExpFac OUT NUMBER
)AS 

BEGIN
  DECLARE
        cusCount NUMBER := NULL;
    BEGIN
        SELECT COUNT(*) INTO cusCount FROM SP_CUS_IDS;
            FOR TEMP_AMT_TAX_A IN (
                        -- เฉพาะยกเว้นเพื่อการส่งออก
                        select    sum(nvl(a.tax_amt,0))  AS  tax_amt  -- ภาษี        
                                , sum(nvl(a.amount,0)) AS amount  -- จำนวน
                        from ed_target.RPT_VW_FACT_APPROVE_ALL a
                        inner join ED_TARGET.IC_OFFICE_DIM OD on OD.OFFCODE=a.OFFCODE
                        where   a.tax_type='ขอยกเว้น'         -- Fix
                                and a.group_id = '0201'      -- Fix
                                and a.time_id >= NVL(to_number(to_char(pFROM_DATE, 'yyyymmdd')), a.time_id)  -- Where Parameter @StartDate
                                and a.time_id <= NVL(to_number(to_char(pTO_DATE, 'yyyymmdd')), a.time_id)  -- Where Parameter @EndDate
                                AND OD.REGION_CD = pREGION_ID          -- Where Parameter
                                and OD.PROVINCE_CD = pAREA_ID        -- Where Parameter
                                and a.OFFCODE = ''              -- Where Parameter
                                and (a.reg_id IN (SELECT CUS_ID FROM SP_CUS_IDS) OR cusCount = 0)           -- Where Parameter
               )
            LOOP
                    dbms_output.put_line('TEMP_AMT_TAX_A :: set output');
                    oAmtTaxExcExpFac := TEMP_AMT_TAX_A.tax_amt;
                    oVolTaxExcExpFac := TEMP_AMT_TAX_A.amount;
            END LOOP;
    END;

END SP_QUERY_DOMEST_TAX_EXCEXPFAC;